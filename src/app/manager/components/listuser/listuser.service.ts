import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/Observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { stringify } from '@angular/core/src/util';
import { HttpErrorResponse } from '@angular/common/http/src/response';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';


@Injectable()
export class ListuserService {
getUrl = '/api/cu/cus';
userSnd = '/api/v1/user';
user = '/api/v1/user';



  constructor(private _http: HttpClient) {  }

  getData(): Observable<any> {
    return this._http.get<any>(this.getUrl )
    .do(data => console.log(JSON.stringify(data)));
  }
}

