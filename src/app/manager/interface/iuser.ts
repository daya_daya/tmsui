export class Iuser {
    constructor (
        public userId?: number ,
        public fullName: string = '',
        public userName: string = '',
        public mobileNo?: number,
        // public address: string= '',
        // public country: string= '',
        // public state: string= '',
        // public city: string= '',
        // public zip?: number,
        public email: string= '',
        public password: string= '',
        public confirm_password: string= '',
        // public packageId: string[]= [],
    ) {}
}
