import { Component } from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { FormGroup,  FormBuilder, Validators , Validator } from '@angular/forms';
import { Iuser } from '../../interface/iuser';
import { HttpClient } from '@angular/common/http';
import { SendUserService } from './send-user.service';





@Component({
  selector: 'app-mm',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {
 userForm: FormGroup;
user: Iuser = new Iuser();
u;
  constructor(private fb: FormBuilder, private _http: SendUserService) { }

  submit(user) {
    // console.log(this.user);
    // console.log('submitted' + JSON.stringify(this.userForm.value));
    this.u = this.userForm.value;
    // console.log(this.u);
    this._http.sendData(this.u).subscribe(res => console.log(res));
  }
  ngOnInit() {

    this.userForm = this.fb.group({
      userId: ['', [Validators.required]],
      fullName: ['', [Validators.required]],
      userName:  ['', [Validators.required]],
      mobileNo: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(10), Validators.maxLength(10)]],
      address:  ['', [Validators.required]],
      country:  ['', [Validators.required]],
      state:  ['', [Validators.required]],
      city:  ['', [Validators.required]],
      zip:  ['', [Validators.required]],
      email:  ['', [Validators.required, Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]')]],
      password: '',
      confirmPassword : '',
      // packageId: '',
    });

 }

}
