import { Component } from '@angular/core';
import { ListuserService } from './manager/components/listuser/listuser.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
 providers: [ListuserService]
})
export class AppComponent {
  title = 'app';
}
