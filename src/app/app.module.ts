import {RouterModule, Routes} from '@angular/router';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule , FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { CheckComponent } from './check/check.component';
import { MaterialModule } from './shared/materail.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CdkTableModule } from '@angular/cdk/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListuserService } from './manager/components/listuser/listuser.service';
import { SendUserService } from './manager/components/adduser/send-user.service';

const route: Routes = [
   {path: '', loadChildren: './welcome/welcome.module#WelcomeModule'},
   {path: '', loadChildren: './manager/manager.module#ManagerModule'},
   {path: '**', redirectTo: ''}
];


@NgModule({
  declarations: [
    AppComponent,
    CheckComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    CdkTableModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(route)],
  providers: [ListuserService,SendUserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
