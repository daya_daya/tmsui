import {Component, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import {CrudComponent} from '../../../shared/crud.component';
import { ListuserService } from './listuser.service';
import { Customer } from '../adduser/customer';

/**
 * @title Data table with sorting, pagination, and filtering.
 */
@Component({
  selector: 'app-listuser',
  templateUrl: './listuser.component.html',
  styleUrls: ['./listuser.component.css']
})

export class ListuserComponent implements OnInit {
  use ;
  users;
  constructor( private _toUsers: ListuserService) { }

  ngOnInit() {
    this._toUsers.getData()
    .subscribe(result => {
      this.users = result.data;
  });
  // this._toUsers.sendData(this.d).subscribe(re => console.log(re));
  }
}
