import {AdduserComponent} from './components/adduser/adduser.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule,  Routes} from '@angular/router';
import {ReactiveFormsModule , FormsModule} from '@angular/forms';
import {CdkTableModule} from '@angular/cdk/table';

import { MaterialModule } from '../shared/materail.module';


import { ListuserComponent } from './components/listuser/listuser.component';
import { CrudComponent } from '../shared/crud.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { SendUserService } from './components/adduser/send-user.service';

const routes: Routes = [
  {path: 'addform', component: AdduserComponent},
  {path: 'listuser', component: ListuserComponent},
      {path: '**', redirectTo: ''}
];


@NgModule({
  imports: [
    CommonModule,
    CdkTableModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AdduserComponent, ListuserComponent , CrudComponent ],
  providers: [SendUserService]
})
export class ManagerModule { }
